import java.util.Scanner;

public class HarrisBenedictEquation {


	public static void main(String[] args) {

		Scanner sc = new Scanner (System.in);
		int weight;
		int height;
		int age;

		System.out.println("Please enter your weight in pounds");
		weight = sc.nextInt();
		System.out.println("Please enter your height in inches");
		height = sc.nextInt();
		System.out.println("Please enter your age in years");
		age = sc.nextInt();

		double caloriesFemale, caloriesMale;

		caloriesFemale = 655 + (4.3*weight) + (4.7*height) - (4.7*age);
		caloriesMale = 66 + (6.3*weight) + (12.9*height) - (6.8*age);

		System.out.println("A woman of " + age + " years, standing " + height + " inches tall, and weighing " + weight + " lbs should eat " + caloriesFemale/230 + " chocolate bars a day to maintain her weight");
		System.out.println("A man of " + age + " years, standing " + height + " inches tall, and weighing " + weight + " lbs should eat " + caloriesMale/230 + " chocolate bars a day to maintain his weight");


	}
}